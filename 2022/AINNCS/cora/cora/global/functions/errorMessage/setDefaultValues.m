function varargout = setDefaultValues(defaultValues,varargin)
% setDefaultValues - set default values for input arguments
%
% Syntax:  
%    finalInputs = setDefaultValues(defaultValues,varargin)
%
% Inputs:
%    defaultValues - {{dV1},{dV2},...,{dV3}}
%
% Outputs:
%    finalInputs - values for input arguments (1xn cell-array), where
%                  the non-given values are set to default values
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: none

% Author:       Mingrui Wang
% Written:      30-May-2022
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------

% parse input arguments
if isempty(varargin)
    n_in = 0;
else
    n_in = length(varargin);
end

% number of default values
n_default = max(size(defaultValues));

% % init output argument
% varargout = cell(1,n_default);

if n_in >= n_default

    varargout = varargin;

else

    for i = 1:n_in
        varargout{i} = varargin{i};
    end
    for i = n_in+1:n_default
        varargout{i} = defaultValues{i}{1};
    end

end

%------------- END OF CODE --------------