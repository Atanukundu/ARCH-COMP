import os
import string
import re
from subprocess import Popen, PIPE


example=[]

# NAV instances....
example.append(['NAV','NAV2-BD2', ' --time-horizon 5 --time-step 0.01 --depth 18 -v x1,x2 -F "loc=9" -e bmc'])
example.append(['NAV','NAV3-BD3', ' --time-horizon 10 --time-step 0.01 --depth 19 -v x1,x2 -F "loc=27" -e bmc'])
example.append(['NAV','NAV4-BD4', ' --time-horizon 10 --time-step 0.01 --depth 15 -v x1,x2  -F "loc=81" -e bmc'])


# ACC instances...
example.append(['ACC','ACCS04-UBD04', ' --time-horizon 5 --time-step 0.1 --depth 6 -v x1,x2  -e bmc'])
example.append(['ACC','ACCS05-UBD05', ' --time-horizon 5 --time-step 0.1 --depth 5 -v x1,x2  -e bmc'])
example.append(['ACC','ACCS06-UBD06', ' --time-horizon 5 --time-step 0.1 --depth 3 -v x1,x2   -e bmc'])
#example.append(['ACC','ACCS07-UBD07', ' --time-horizon 5 --time-step 0.1 --depth 5 -v x1,x2  -e bmc'])
#example.append(['ACC','ACCS08-UBD08', ' --time-horizon 5 --time-step 0.1 --depth 1 -v x1,x2  -e bmc'])

# DISC instances...
example.append(['DISC','DISC02-UBS02',  ' --time-step 0.1 --time-horizon 5 --depth 30 -v scheduler_x1,scheduler_x2 -e bmc'])
example.append(['DISC','DISC03-UBS03',  ' --time-step 0.1 --time-horizon 5 --depth 30 -v scheduler_x1,scheduler_x2 -e bmc'])
#example.append(['DISC','DISC04-UBS04',  ' --time-step 0.1 --time-horizon 5 --depth 30 -v scheduler_x1,scheduler_x2  -e bmc'])
#example.append(['DISC','DISC05-UBS05',  ' --time-step 0.1 --time-horizon 5 --depth 30 -v scheduler_x1,scheduler_x2  -e bmc'])



# results in ./result
if not os.path.isdir('result'):
    os.mkdir('result')
    # populate the results.csv file	
with open("./result/results.csv","w") as f:
   f.write('Tool, Benchmark, Instance, Safety Status, Time\n')
   for i in range(0,len(example)):
       string_cmd = 'docker run satreach -m ../benchmarks/PCDB/' + example[i][0] +'/'+ example[i][1] + '.xml -c ../benchmarks/PCDB/' + example[i][0]+'/'+ example[i][1] + '.cfg' + example[i][2]
       print('\nrunning '+ example[i][1])
       p = Popen(string_cmd, stdout=PIPE, stderr=PIPE, stdin=PIPE, shell=True)
       output = str(p.communicate())
       safety = "Model is SAFE"	
       if output.find(safety)== -1:
        safety_res = 0
       else: 
        safety_res = 1
       time_str = "User Time  (in Seconds) ="
       r_idx = output.find(time_str)
       r_idx_time_start = int(r_idx)+len(time_str)+1
       r_idx_time_end = int(r_idx)+len(time_str)+8
       
       if r_idx >= 0:
        output_time = output[r_idx_time_start:r_idx_time_end];
        # remove non-numeric characters (except .)
        output_time = re.sub("[^\d\.]", "", output_time)
        f.write('SAT-Reach, '+example[i][0]+', ' + example[i][1]+ ',' + str(safety_res)+', '+output_time+'\n')
f.close()        
os.chdir('..')
print("results.csv file of SAT-Reach created successfully :)\n")


