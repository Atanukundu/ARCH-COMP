\normalsize
Zonotopes are a very efficient representation for reachability analysis of linear systems \cite{Girard2005} and of nonlinear systems that can be well abstracted by linear differential inclusions \cite{Althoff2010a}. However, more advanced techniques, such as in \cite{Althoff2013a}, abstract more accurately to nonlinear difference inclusions. As a consequence, linear maps of reachable sets are replaced by nonlinear maps. Zonotopes are not closed under nonlinear maps and are not particularly good at over-approximating them. For this reason, polynomial zonotopes are introduced in \cite{Althoff2013a}. Polynomial zonotopes are a new non-convex set representation and can be efficiently stored and manipulated. The new representation shares many similarities with Taylor models \cite{Hoefkens2001} (as briefly discussed later) and is a generalization of zonotopes. Please note that a zonotope cannot be represented by a Taylor model.

Given a \textit{starting point} $c\in\mathbb{R}^{n}$, multi-indexed \textit{generators} $f\^{[i],j,k,\ldots,m} \in\mathbb{R}^{n}$, and single-indexed \textit{generators} $g\^{i} \in\mathbb{R}^{n}$, a polynomial zonotope is defined as
 \begin{gather} 
  \mathcal{PZ} = \Big\{ c + \sum_{j=1}^{p} \beta_j f\^{[1],j} + \sum_{j=1}^{p} \sum_{k=j}^{p} \beta_j \beta_k f\^{[2],j,k} 
 + \ldots + \sum_{j=1}^{p} \sum_{k=j}^{p} \ldots \sum_{m=l}^{p} \underbrace{\beta_j \beta_k \ldots \beta_m}_{\eta \text{ factors }} f\^{[\eta],j,k,\ldots,m} \notag \\ 
 + \sum_{i=1}^{q} \gamma_i g\^{i} \Big| \beta_i, \gamma_i \in [-1,1] \Big\}. \label{eq:polynomialZonotope}
\end{gather}
The scalars $\beta_i$ are called \textit{dependent factors}, since changing their values does not only affect the multiplication with one generator, but with other generators too. On the other hand, the scalars $\gamma_i$ only affect the multiplication with one generator, so they are called \textit{independent factors}. The number of dependent factors is $p$, the number of independent factors is $q$, and the polynomial order $\eta$ is the maximum power of the scalar factors $\beta_i$. The order of a polynomial zonotope is defined as the number of generators $\xi$ divided by the dimension, which is  $\rho = \frac{\xi}{n}$. For a concise notation and later derivations, we introduce the matrices
\begin{equation*}
 \begin{split}
  E^{[i]} = [& \underbrace{f\^{[i],1,1,\ldots,1}}_{=:e\^{[i],1}} \, \ldots \, \underbrace{f\^{[i],p,p,\ldots,p}}_{=:e\^{[i],p}} ] \text{ (all indices are the same value)}, \\
  F^{[i]} = [& f\^{[i],1, 1, \ldots, 1, 2} \, f\^{[i],1, 1, \ldots, 1, 3} \, \ldots \, f\^{[i],1, 1, \ldots, 1, p} \\
           & f\^{[i],1, 1, \ldots, 2, 2} \, f\^{[i],1, 1, \ldots, 2, 3} \, \ldots \, f\^{[i],1, 1, \ldots, 2, p} \\
           & f\^{[i],1, 1, \ldots, 3, 3} \ldots ] \text{ (not all indices are the same value)}, \\
  G = [& g\^{1} \, \ldots \, g\^{q} ],
 \end{split}
\end{equation*}
and $E=\begin{bmatrix} E^{[1]} & \ldots & E^{[\eta]} \end{bmatrix}$, $F=\begin{bmatrix} F^{[2]} & \ldots & F^{[\eta]} \end{bmatrix}$ ($F^{[i]}$ is only defined for $i\geq 2$). Note that the indices in $F^{[i]}$ are ascending due to the nested summations in \eqref{eq:polynomialZonotope}. In short form, a polynomial zonotope is written as $\mathcal{PZ} = (c,E,F,G)$. 

For a given polynomial order $i$, the total number of generators in $E^{[i]}$ and $F^{[i]}$ is derived using the number $\binom{p+i-1}{i}$ of combinations of the scalar factors $\beta$ with replacement (i.e., the same factor can be used again). Adding the numbers for all polynomial orders and adding the number of independent generators $q$, results in $\xi=\sum_{i=1}^\eta \binom{p+i-1}{i} + q$ generators, which is in $\mathcal{O}(p^\eta)$ with respect to $p$. The non-convex shape of a polynomial zonotope with polynomial order $2$ is shown in Fig. \ref{fig:polynomialZonotope}.

\begin{figure}[htb]
  \centering
  \footnotesize

  \psfrag{a}[c][c]{$x_1$}
  \psfrag{b}[c][c]{$x_2$}
  \psfrag{c}[l][c]{\shortstack[l]{polynomial zonotope \\ $\mathcal{PZ}=(\mathbf{0},E,F,G)$}}
  \psfrag{d}[l][c]{sample}
  \psfrag{e}[r][c]{\shortstack[l]{$E^{[1]} = \begin{bmatrix} -1 & 0 \\ 0 & 0.5 \end{bmatrix}$ \\
                                  $E^{[2]} = \begin{bmatrix} 1 & 1 \\ 0.5 & 0.3 \end{bmatrix}$ \\
                                  $F^{[2]} = \begin{bmatrix} -0.5 \\ 1 \end{bmatrix}$ \\
                                  $G = \begin{bmatrix} 0.3 \\ 0.3 \end{bmatrix}$}}
  \psfrag{f}[r][c]{$3$}
  \psfrag{g}[r][c]{$2$}
  \psfrag{h}[r][c]{$1$}
  \psfrag{i}[r][c]{$0$}
  \psfrag{j}[r][c]{$-1$}
  \psfrag{k}[c][c]{$0$}
  \psfrag{l}[c][c]{$2$}
  \psfrag{m}[c][c]{$4$}
    \includegraphics[width=0.7\columnwidth]{./figures/examplePolynomialZonotope_1e3_bw_mixed_c.eps}
  \caption{Over-approximative plot of a polynomial zonotope as specified in the figure. Random samples of possible values demonstrate the accuracy of the over-approximative plot.}
  \label{fig:polynomialZonotope}
\end{figure}

So far, polynomial zonotopes are only implemented up to polynomial order $\eta=2$ so that the subsequent class is called \texttt{quadZonotope} due to the quadratic polynomial order. 
We support the following methods for the \texttt{quadZonotope} class:
\begin{itemize}
 \item \texttt{cartesianProduct} -- returns the Cartesian product of a \texttt{quadZonotope} and a \texttt{zonotope}.
 \item \texttt{center} -- returns the starting point $c$.
 \item \texttt{display} -- standard method, see Sec. \ref{sec:setRepresentationsAndOperations}.
 \item \texttt{enclose} -- generates an over-approximative \texttt{quadZonotope} that encloses two \texttt{quadZonotopes} of equal dimension by first over-approximating them by zonotopes and subsequently applying \texttt{enclose} of the \texttt{zonotope} class.
 \item \texttt{enclosingPolytope} -- returns an over-approximating polytope in halfspace representation by first over-approximating by a \texttt{zonotope} object and subsequently applying its \texttt{enclosingPolytope} method.
 \item \texttt{generators} -- returns the generators of a \texttt{quadZonotope}.
 \item \texttt{interval} -- standard method, see Sec. \ref{sec:setRepresentationsAndOperations}. The interval hull is obtained by over-approximating the \texttt{quadZonotope} by a \texttt{zonotope} and subsequent application of its \texttt{interval} method. Other than for the \texttt{zonotope} class, the generated interval hull is not tight in the sense that it touches the \texttt{quadZonotope}.
 \item \texttt{intervalhullAccurate} -- over-approximates a \texttt{quadZonotope} by a tighter interval hull as when applying \texttt{interval}. The procedure is based on splitting the \texttt{quadZonotope} in parts that can be more faithfully over-approximated by interval hulls. The union of the partially obtained interval hulls constitutes the result.
 \item \texttt{mtimes} -- standard method, see Sec. \ref{sec:setRepresentationsAndOperations} as stated in \cite[Equation 14]{Althoff2011f} for numeric matrix multiplication. As described in Sec. \ref{sec:zono_mtimes} the multiplication of interval matrices is also supported, whereas the implementation for matrix zonotopes is not yet implemented.
 \item \texttt{mptPolytope} -- computes a mptPolytope object that encloses the quadZonotope object.
 \item \texttt{plot} -- standard method, see Sec. \ref{sec:setRepresentationsAndOperations}. More details can be found in Sec. \ref{sec:plotting}.
 \item \texttt{plotFilled} -- standard method, see Sec. \ref{sec:setRepresentationsAndOperations}. More details can be found in Sec. \ref{sec:plotting}.
 \item \texttt{plus} -- standard method, see Sec. \ref{sec:setRepresentationsAndOperations}. Addition is realized for \texttt{quadZonotope} objects with MATLAB vectors, \texttt{zonotope} objects, and \texttt{quadZonotope} objects.
 \item \texttt{pointSet} -- computes a user-defined number of random points within the \texttt{quadZonotope}.
 \item \texttt{pointSetExtreme} -- computes a user-defined number of random points when only allowing the values $\{-1,1\}$ for $\beta_i, \gamma_i$ (see \eqref{eq:polynomialZonotope}).
 \item \texttt{project} -- returns a \texttt{quadZonotope}, which is the projection of the input argument onto the specified dimensions.
 \item \texttt{quadraticMultiplication} -- given a \texttt{quadZonotope} $\mathcal{Z}$ and a discrete set of matrices $Q\^i\in\mathbb{R}^{n\times n}$ for $i=1\ldots n$, \texttt{quadraticMultiplication} computes $\{\varphi | \varphi_i=x^T Q\^i x, \, x\in\mathcal{Z}\}$ as described in \cite[Corollary 1]{Althoff2013a}.
 \item \texttt{quadZonotope} -- constructor of the class.
 \item \texttt{randPoint} -- computes a random point within the \texttt{quadZonotope}.
 \item \texttt{randPointExtreme} -- computes a random point when only allowing the values $\{-1,1\}$ for $\beta_i, \gamma_i$ (see \eqref{eq:polynomialZonotope}).
 \item \texttt{reduce} -- returns an over-approximating \texttt{quadZonotope} with less generators as detailed in Sec. \ref{sec:quadZono_reduce}.
 \item \texttt{splitLongestGen} -- splits the longest generator factor and returns two \texttt{quadZonotope} objects whose union encloses the original \texttt{quadZonotope} object.
 \item \texttt{splitOneGen} -- splits one generator factor and returns two \texttt{quadZonotope} objects whose union encloses the original \texttt{quadZonotope} object.
 \item \texttt{zonotope} -- computes an enclosing zonotope as presented in \cite[Proposition 1]{Althoff2013a}.
\end{itemize}


\subsubsection{Method \texttt{reduce}} \label{sec:quadZono_reduce}

The zonotope reduction returns an over-approximating zonotope with less generators. Table \ref{tab:polyZono_reduction} lists the implemented reduction techniques. 

\begin{table}[h]
\caption{Reduction techniques for zonotopes.}
\begin{center}\label{tab:polyZono_reduction}
\begin{tabular}{lll}
	\hline 
	\textbf{reduction} & & \\
	\textbf{technique} & \textbf{comment} & \textbf{literature} \\ \hline
	\texttt{girard} & Only changes independent generators  & \cite[Sec. 3.4]{Girard2005} \\ 
	& as for a regular zonotope &  \\ 
	\texttt{redistGirard} & Combines the techniques \texttt{girard} and \texttt{redistribute} & $-$ \\
	\texttt{redistribute} & Changes dependent and independent generators & \cite[Proposition 2]{Althoff2013a} \\
	\hline
\end{tabular}
\end{center}
\end{table}


\subsubsection{Polynomial Zonotope Example} \label{sec:polynomialZonotopeExample}

The following MATLAB code demonstrates some of the introduced methods:

{\small
\input{./MATLABcode/example_quadZonotope.tex}}

This produces the workspace output
\begin{verbatim}
id: 0
dimension: 2
c: 
    1.0000
    0.4000

g_i: 
   -1.0000         0    0.5000    0.5000   -0.5000    0.3000
         0    0.5000    0.2500    0.1500    1.0000    0.3000
\end{verbatim}

The plot generated in lines 11-12 is shown in Fig. \ref{fig:quadZonotopeExample}.

\begin{figure}[h!tb]
  \centering
  %\footnotesize
  \psfrag{a}[c][c]{$x_1$}
  \psfrag{b}[c][c]{$x_2$}
    \includegraphics[width=0.8\columnwidth]{./figures/setRepresentationTest/quadZonotopeTest_1.eps}
  \caption{Sets generated in lines 11-12 of the polynomial zonotope example in Sec. \ref{sec:polynomialZonotopeExample}.}
  \label{fig:quadZonotopeExample}
\end{figure}
