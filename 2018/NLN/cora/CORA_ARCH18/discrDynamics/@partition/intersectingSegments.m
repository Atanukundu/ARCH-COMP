function [segments,error]=intersectingSegments(Obj,contSet,varargin)
% findSegments: returns the segments possibly intersecting with a
% continuous set, overapproximatively, by overapproximating the convex set 
% as a multidimensional interval (AABB).
%
% Input:
% 1st input:            Interval object
% 2nd input:            a continuous set
% 3rd input (optional): 'subscripts' gives answer as subscripts of the 
%                       partition segments, 'indices' gives the answer as 
%                       indices. Default is indices.
%
% Output:          
% segments:             either the indices or the subscripts of the
%                       possibly intersecting cells
% error:                error flag
%
% Checked:  2.8.17, AP

if nargin > 2
    switch varargin{1}
        case 'subscripts'
            giveAnswerAsIndices = 0;
        case 'indices'
            giveAnswerAsIndices = 1;
        otherwise
            disp('invalid choice of output, defaulting to indices')
            giveAnswerAsIndices = 1;
    end
else
    % default case is indices
    giveAnswerAsIndices = 1;
end


if isa(contSet,'zonotope')
    I=interval(contSet);
elseif isa(contSet,'polytope')
    V=vertices(extreme(contSet)');
    I=interval(V);    
elseif isa(contSet,'mptPolytope')
    I=interval(contSet);  
else
    I=contSet;
end


error=0; % set error to 0
if isa(I, 'interval')
    leftLimit = infimum(I);
    rightLimit = supremum(I);

else
    leftLimit = I;
    rightLimit = I;
end

if (size(I,2) ~= 1)
    if (size(I,1) ~= 1)
        disp('either define an interval or a point')
        return
    else
        leftLimit = leftLimit';
        rightLimit = rightLimit';
    end
end

bounds = [leftLimit,rightLimit];

if size(bounds,1) ~= length(Obj.dividers)
    disp('state space and subset are not of same dimension')
    return
end

currentIndex=cell(length(Obj.dividers),1);
for iDim= 1:length(Obj.dividers)
    lower=bounds(iDim,1);
    upper=bounds(iDim,2);
    % find those above the lower bound
    E=Obj.dividers{iDim}>=lower;
    % find those above the upper bound
    F=Obj.dividers{iDim}<=upper;
    % bitshift and add it to the list of those already there
    currentIndex{iDim}=unique([currentIndex{iDim},find(E(2:end)&F(1:(end-1)))]);
   % if isempty(currentIndex{iDim})||E(1)||F(end)
    if isempty(currentIndex{iDim})
        error=1;
    end
end

MX = zeros(1,0);
for iDim=1:length(currentIndex)
    MX=[repmat(MX,length(currentIndex{iDim}),1),reshape(repmat(currentIndex{iDim},size(MX,1),1),[],1)];
end
Multiples=ones(length(currentIndex),1);
for i = 1:(length(currentIndex)-1)
    Multiples(i,1)=prod(Obj.nrOfSegments((i+1):end));
end
segments=(MX-1)*Multiples;


if isempty(currentIndex)||error
    segments=[0];
elseif giveAnswerAsIndices
    segments=s2i(Obj,MX);
    
    if sum(bounds(:,1)<Obj.intervals(:,1))||sum(bounds(:,2)>Obj.intervals(:,2))
        segments = [segments,0];
    end
else
    segments=MX;
    if sum(bounds(:,1)<Obj.intervals(:,1))||sum(bounds(:,2)>Obj.intervals(:,2))
        segments = [segments;zeros(1,length(Obj.nrOfSegments))];
    end
end

end