function table = table_authors(directory)
% table_authors - generates a table as a categorical matrix. The rows are the
% name of the authors. The columns are the names of the files. The content
% of each cell is the number of contributions of each author in each file.
% The symbol '-' indicates no contribution. 
%
% Syntax:  
%    table_authors(directory)
%
% Inputs:
%       directory: a string contains the full directory of the CORA files.
%
% Outputs:
%       table: a categorical matrix containing the table as in the 
%       description above.
% 
% Author:       Raja Judeh
% Written:      February-2018
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------

%% Extracting the main files in the directory

files = dir(directory);
files = files([files.isdir]); %extracting the files that are directories
files(1:3) = []; %remove the first 3 unwanted files
files(8) = []; %remove the 'manual' directory since it contains no m. files
num_files = length(files); %number of the main directories (files)

%% Loading the names of all the authors who contributed in all the files

names = load('acceptable_names.mat'); 
names = names.acceptable_names;
num_names = length(names);

%% Initializing the table as a categorical matrix

table = categorical(zeros(num_names+1, num_files+1));
table(:) = '-'; %filling all the table with the symbol '-'
table(1,1) = 'Names/Files'; %filling the first cell
table(2:end,1) = names; %filling the first column with the authors' names

%% Filling the table with the number of contributions of each author

for f_idx = 1:num_files
    sub_directory = [files(f_idx).folder, '\', files(f_idx).name];
    
    % Fill the first row with the files' names
    table(1,f_idx+1) = files(f_idx).name; 
    
    % Generate the histcounts of the subdirectory
    HC = histcounts_authors(sub_directory, false);   
    num_authors = size(HC, 2); %number of authors contributed in this file
    
    % Find the indices of contributing authors in the table and fill in. 
    % Looping over all the contributing authors of the current file.
    for auth_idx = 1:num_authors 
        c_idx = table(:,1) == HC(1,auth_idx); 
        table(c_idx, f_idx+1) = HC(2,auth_idx);
    end
end

%% Print the table as a string in latex form

print_table(table) 

end

%% Function: print_table

function print_table(table)

%print_table: prints the table, given as a categorical matrix, as a string.
%The output has the latex table form.

[nR, nC] = size(table);
latex = '';

for r = 1:nR
    if r ~= 1
        latex = latex + ' \\\\' + '\n';
    end
    
    for c = 1:nC
        if c == 1
            latex = latex + string(table(r,c));
        else
            latex = latex + ' & ' + string(table(r,c));
        end
    end
end

sprintf(latex)

%------------- END OF CODE --------------

end

