#!/bin/sh

docker build -t keymaerax480:1.0 .
docker create -it --name kyx480 keymaerax480:1.0 bash
docker start kyx480
docker exec -it kyx480 wolframscript "-activate"
docker cp kyx480:/root/.WolframEngine/Licensing .
docker rm -f kyx480
