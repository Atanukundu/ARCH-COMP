function [Rout,Rout_tp,res,tVec] = reach(obj,params,options)
% reach - computes the reachable set for linear system
%
% Syntax:  
%    [Rout,Rout_tp,res] = reach(obj,params,options)
%
% Inputs:
%    obj - continuous system object
%    params - model parameters
%    options - options for the computation of reachable sets
%
% Outputs:
%    Rout - reachable set of time intervals for the continuous dynamics
%    Rout_tp - reachable set of time points for the continuous dynamics
%    res  - boolean (only if some specification checked)
%    tVec - vector of time steps
%
% Example: 
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: none

% Author:        Mark Wetzlinger
% Written:       26-June-2019
% Last update:   08-Oct-2019
%                23-April-2020 (added params)
% Last revision: ---


%------------- BEGIN CODE --------------

% options preprocessing
options = params2options(params, options);
options = checkOptionsReach(obj,options);

% decide which reach function to execute by options.alg
if strcmp(options.linAlg,'adap')
    [Rout,Rout_tp,res,tVec] = reach_adaptive(obj, options);
else
    % all below, const. time step sizes
    tVec = options.timeStep;
    if strcmp(options.linAlg,'standard')
        [Rout,Rout_tp,res] = reach_standard(obj, options);
    elseif strcmp(options.linAlg,'wrapping-free')
        [Rout,Rout_tp,res] = reach_wrappingfree(obj, options);
    elseif strcmp(options.linAlg,'fromStart')
        [Rout,Rout_tp,res] = reach_fromStart(obj, options);
    elseif strcmp(options.linAlg,'decomp')
        [Rout,Rout_tp,res] = reach_decomp(obj, options);
    elseif strcmp(options.linAlg,'krylov')
        [Rout,Rout_tp,res] = reach_krylov(obj, options);
    end
end


end


%------------- END OF CODE --------------