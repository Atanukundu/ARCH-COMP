A disadvantage of zonotopes is that they are not closed under intersection, i.e., the intersection of two zonotopes does not return a zonotope in general. In order to overcome this disadvantage, zonotope bundles are introduced in \cite{Althoff2011f}. Given a finite set of zonotopes $\mathcal{Z}_i$, a zonotope bundle is $\mathcal{Z}^\cap = \bigcap_{i=1}^{s} \mathcal{Z}_i$, i.e., the intersection of zonotopes $\mathcal{Z}_i$. Note that the intersection is not computed, but the zonotopes $\mathcal{Z}_i$ are stored in a list, which we write as $\mathcal{Z}^\cap = \{ \mathcal{Z}_1, \ldots, \mathcal{Z}_s\}^\cap$. 

We support the following methods for zonotope bundles:
\begin{itemize}
 \item \texttt{and} -- returns the intersection with a zonotope bundle or a zonotope.
 \item \texttt{cartesianProduct} -- returns the Cartesian product of a zonotope bundle with a zonotope.
 \item \texttt{conZonotope} -- convert a zonotope bundle into a constrained zonotope.
 \item \texttt{display} -- standard method, see Sec. \ref{sec:setRepresentationsAndOperations}.
 \item \texttt{enclose} -- generates a zonotope bundle that encloses two zonotopes bundles of equal dimension according to \cite[Proposition 5]{Althoff2011f}.
 \item \texttt{encloseTight} -- generates a zonotope bundle that encloses two zonotopes bundles in a possibly tighter way than \texttt{enclose} as outlined in \cite[Sec. VI.A]{Althoff2011f}.
 \item \texttt{enclosingPolytope} -- returns an over-approximating polytope in halfspace representation. For each zonotope the method \texttt{enclosingPolytope} of the class \texttt{zonotope} in Sec. \ref{sec:zonotope} is called.
 \item \texttt{enlarge} -- enlarges the generators of each zonotope in the bundle by a vector of factors for each dimension.
 \item \texttt{interval} -- standard method, see Sec. \ref{sec:setRepresentationsAndOperations}. More details can be found in \cite[Proposition 6]{Althoff2011f}.
 \item \texttt{mtimes} -- standard method, see Sec. \ref{sec:setRepresentationsAndOperations}. More details can be found in \cite[Proposition 1]{Althoff2011f}.
 \item \texttt{plot} -- standard method, see Sec. \ref{sec:setRepresentationsAndOperations}. More details can be found in Sec. \ref{sec:plotting}.
 \item \texttt{plotFilled} -- standard method, see Sec. \ref{sec:setRepresentationsAndOperations}. More details can be found in Sec. \ref{sec:plotting}.
 \item \texttt{plus} -- standard method, see Sec. \ref{sec:setRepresentationsAndOperations}. More details can be found in \cite[Proposition 2]{Althoff2011f}.
 \item \texttt{polytope} -- returns an exact polytope in halfspace representation. Each zonotope is converted to halfspace representation according to \cite[Theorem 2.1]{Althoff2010a} and later all obtained H polytopes are intersected. 
 \item \texttt{project} -- returns a zonotope bundle, which is the projection of the input argument onto the specified dimensions.
 \item \texttt{quadraticMultiplication} -- given a zonotope bundle $\mathcal{Z}^\cap$ and a discrete set of matrices $Q\^i\in\mathbb{R}^{n\times n}$ for $i=1\ldots n$, \texttt{quadraticMultiplication} computes $\{\varphi | \varphi_i=x^T Q\^i x, \, x\in\mathcal{Z}^\cap\}$ as described in \cite[Lemma 1]{Althoff2014a}.
 \item \texttt{randPoint} -- generates a random point within a zonotope bundle.
 \item \texttt{randPointExtreme} -- generates a random extreme point of a zonotope bundle.
 \item \texttt{reduce} -- returns an over-approximating zonotope bundle with less generators. For each zonotope the method \texttt{reduce} of the class \texttt{zonotope} in Sec. \ref{sec:zonotope} is called. 
 \item \texttt{reduceCombined} -- reduces the order of a zonotope bundle by not reducing
each zonotope separately as in \texttt{reduce}, but in a combined fashion.
 \item \texttt{shrink} -- shrinks the size of individual zonotopes by explicitly computing the intersection of individual zonotopes; however, in total, the size of the zonotope bundle will increase. This step is important when individual zonotopes are large, but the zonotope bundles represents a small set. In this setting, the over-approximations of some operations, such as \texttt{mtimes} might become too over-approximative. Although \texttt{shrink} initially increases the size of the zonotope bundle, subsequent operations are less over-approximative since the individual zonotopes have been shrunk.
 \item \texttt{split} -- splits a zonotope bundle into two or more zonotopes bundles. Other than for zonotopes, the split is exact. The method can split halfway in a particular direction or given a separating hyperplane.
 \item \texttt{vertices} -- returns potential vertices of a zonotope bundle (WARNING: Do not use this function for high order zonotope bundles due to high computational complexity).
 \item \texttt{volume} -- computes the volume of a zonotope bundle by converting it to a polytope using \texttt{polytope} and using a volume computation for polytopes.
 \item \texttt{zonotopeBundle} -- constructor of the class.
\end{itemize}

\subsubsection{Zonotope Bundle Example} \label{sec:zonotopeBundleExample}

The following MATLAB code demonstrates some of the introduced methods:

{\small
\input{./MATLABcode/example_zonotopeBundle.tex}}

This produces the workspace output
\begin{verbatim}
vol =

    1.0000
\end{verbatim}

The plot generated in lines 7-9 is shown in Fig. \ref{fig:zonotopeBundleExample}.

\begin{figure}[h!tb]
  \centering
  %\footnotesize
  \psfrag{a}[c][c]{$x_1$}
  \psfrag{b}[c][c]{$x_2$}
    \includegraphics[width=0.8\columnwidth]{./figures/setRepresentationTest/zonotopeBundleTest_1.eps}
  \caption{Sets generated in lines 7-9 of the zonotope bundle example in Sec. \ref{sec:zonotopeBundleExample}.}
  \label{fig:zonotopeBundleExample}
\end{figure}