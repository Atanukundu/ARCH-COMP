function [functionName,HA] = data2ParallelHA( Data, path)

%INPUT :
%    Data : Automaton in structHA formate
%    path: folder from which to generate auxillary files
%OUTPUT :
%     HA: Automaton in CORA format as a string
%


% Get Meta Information of the given Automaton
Components = Data.Components;
functionName = Data.name;
automaton_id = Data.componentID;

% Create main comments
functionStr = "function HA = " + functionName + "(~)";
dateComment = "%% Generated on " + datestr(date);
aCommentStr = padComment("Automaton created from Component '" + automaton_id + "'");
automatonStr = functionStr + newlines(3) + dateComment + newlines(2) + aCommentStr + newlines(2);

% Create Interface information comment
infoCommentStr = "%% Interface Specification:" + newline +...
    "%   This section clarifies the meaning of state, input & output dimensions" + newline +...
    "%   by showing their mapping to SpaceEx variable names. " + newlines(2);

% For each component list variable names of state & input space
numberOfComp = length(Components);
infoStr = "";
for comp = 1:numberOfComp
    Comp = Components{comp};
    
    compInfoStr = "% Component " + int2str(comp) + " (" + Comp.name + "):" + newline;
    
    % gather state names in string array
    stateNames = [Comp.states.name];
    % print to comment
    stateStr = "%  state x := [" + stateNames(1);
    if(length(stateNames) > 1)
        stateStr = stateStr + sprintf("; %s",stateNames(2:end));
    end
    stateStr = stateStr + "]" + newline;
    
    % gather input names in string array
    inputNames = [Comp.inputs.name];
    % print to comment
    inputStr = "%  input u := [" + inputNames(1);
    if(length(inputNames) > 1)
        inputStr = inputStr + sprintf("; %s",inputNames(2:end));
    end
    inputStr = inputStr + "]" + newline;
    
    if ~isempty(Comp.outputs)
        % gather output names in string array
        outputNames = [Comp.outputs.name];
        % print to comment
        outputStr = "%  output y := [" + outputNames(1);
        if(length(outputNames) > 1)
            outputStr = outputStr + sprintf("; %s",outputNames(2:end));
        end
        outputStr = outputStr + "]" + newlines(2);
    else
        outputStr = newline;
    end
    
    infoStr = infoStr + compInfoStr + stateStr + inputStr + outputStr;
end

automatonStr = automatonStr + infoCommentStr + infoStr;

componentStr = "";
% for stateBinds of pHA
totalStates = 0;
% For each component in automaton
for comp = 1:numberOfComp
    
    Comp = Components{comp};
    
    %Get Meta Information for the Component "comp"
    component_id = Comp.name;
    States = Comp.States;
    
    % Write Comment for Component "comp"
    cCommentStr = padComment("Component " + component_id) + newlines(2);
    % Append it to component String
    componentStr = componentStr + cCommentStr;
    
    
    % For each state in component
    numberOfStates = length(States);
    for state = 1:numberOfStates
        State = States(state);
        
        % Write Comment for State "state"
        sCommentStr = padComment("State " + State.name) + newlines(2);
        stateStr = sCommentStr;
        
        % Give original equation as comment
        dynamicsC = text2comment("equation:" + newline + State.Flow.Text) + newline;
        if isfield(State.Flow,'A')
            % Get information for linear system
            linSysA = printMatrixConverter(State.Flow.A);
            linSysAStr = "dynA = ..." + newline + linSysA + ";" + newline;
            linSysB = printMatrixConverter(State.Flow.B);
            linSysBStr = "dynB = ..." + newline + linSysB + ";" + newline;
            linSysc = printMatrixConverter(State.Flow.c);
            linSyscStr = "dync = ..." + newline + linSysc + ";" + newline;
            % Get information about outputs from Invariant of linear system
            if isempty(Comp.outputs)
                % no outputs, no equation of the form y = Cx + Du + k
                dynamicsStr = dynamicsC + linSysAStr + linSysBStr + linSyscStr + ...
                    "dynamics = linearSys('linearSys', dynA, dynB, dync);" + newlines(2);
            else
                % include output equation y = Cx + Du + k
                linSysC = printMatrixConverter(State.Invariant.C);
                linSysCStr = "dynC = ..." + newline + linSysC + ";" + newline;
                linSysD = printMatrixConverter(State.Invariant.D);
                linSysDStr = "dynD = ..." + newline + linSysD + ";" + newline;
                linSysk = printMatrixConverter(State.Invariant.k);
                linSyskStr = "dynk = ..." + newline + linSysk + ";" + newline;
                dynamicsStr = dynamicsC + linSysAStr + linSysBStr + linSyscStr + ...
                    linSysCStr + linSysDStr + linSyskStr + ...
                    "dynamics = linearSys('linearSys', dynA, dynB, dync, dynC, dynD, dynk);" + newlines(2);
            end
        else
            % choose name for dynamics function
            if numberOfComp==1
                % simplify names for monolithic automata
                nonlinName = sprintf("%s_St%d_FlowEq",functionName,state);
            else
                nonlinName = sprintf("%s_C%d_St%d_FlowEq",functionName,comp,state);
            end
            
            dynOptStr = "dynOpt = struct('tensorOrder',1);" + newline;
            
            % find dynamics of system
            statedims = num2str(length(Comp.states));
            inputdims = num2str(length(Comp.inputs));
            
            printDynamicsFile(path,nonlinName,State.Flow.FormalEqs);
            
            dynamicsStr = dynamicsC + dynOptStr + "dynamics = nonlinearSys(" +...
                statedims + "," + inputdims + ",@" + nonlinName + ",dynOpt); " + newlines(2);
        end
        
        % Get information for Invariant
        invariantA = printMatrixConverter(State.Invariant.A);
        invariantAStr = "invA = ..." + newline + invariantA + ";" + newline;
        invariantb = printMatrixConverter(State.Invariant.b);
        invariantbStr = "invb = ..." + newline + invariantb + ";" + newline;
        invOptStr = "invOpt = struct('A', invA, 'b', invb";
        if ~isempty(State.Invariant.Ae)
            % if invariant is a polyhedron, add additional parameters
            invariantAe = printMatrixConverter(State.Invariant.Ae);
            invariantAeStr = "invAe = ..." + newline + invariantAe + ";" + newline;
            invariantbe = printMatrixConverter(State.Invariant.be);
            invariantbeStr = "invbe = ..." + newline + invariantbe + ";" + newline;
            invOptStr = invOptStr + ",'Ae', invAe, 'be', invbe";
        else
            invariantAeStr = "";
            invariantbeStr = "";
        end
        invOptStr = invariantAStr + invariantbStr + invariantAeStr + invariantbeStr + ...
                    invOptStr + ");" + newline;
        
        % Write String for Invariant
        invariantC = text2comment("equation:" + newline + State.Invariant.Text) + newline;
        invariantStr = invariantC + invOptStr + "inv = mptPolytope(invOpt);" + newlines(2);
        
        transitionStr = "trans = {};" + newline;
        % For each Transition
        Trans = State.Trans;
        numberOfTrans = length(Trans);
        for trans = 1:numberOfTrans
            Tran = Trans(trans);
            
            % Get information for destination for Transition "trans"
            transDestination = num2str(Tran.destination);
            
            % Get Information for Reset for Transition "trans"
            resetA = printMatrixConverter(Tran.reset.A);
            resetAStr = "resetA = ..." + newline + resetA + ";" + newline;
            resetb = printMatrixConverter(Tran.reset.b);
            resetbStr = "resetb = ..." + newline + resetb + ";" + newline;
            
            % Write Reset String
            resetC = text2comment("equation:" + newline + Tran.reset.Text) + newline;
            resetStr = resetC + resetAStr + resetbStr + ...
                    "reset = struct('A', resetA, 'b', resetb);" + newlines(2);
            
            % Get Information for Guards for Transition "trans"
            guardA = printMatrixConverter(Tran.guard.A);
            guardAStr = "guardA = ..." + newline + guardA + ";" + newline;
            guardb = printMatrixConverter(Tran.guard.b);
            guardbStr = "guardb = ..." + newline + guardb + ";" + newline;
            guardOptStr = "guardOpt = struct('A', guardA, 'b', guardb";
            if ~isempty(Tran.guard.Ae)
                % if guard is a polyhedron, add additional parameters
                guardAe = printMatrixConverter(Tran.guard.Ae);
                guardAeStr = "guardAe = ..." + newline + guardAe + ";" + newline;
                guardbe = printMatrixConverter(Tran.guard.be);
                guardbeStr = "guardbe = ..." + newline + guardbe + ";" + newline;
                guardOptStr = guardOptStr + ", 'Ae', guardAe, 'be', guardbe";
            else
                guardAeStr = "";
                guardbeStr = "";
            end
            guardOptStr = guardAStr + guardbStr + guardAeStr + guardbeStr + ...
                        guardOptStr + ");" + newline;
            
            % Write Guard String
            guardC = text2comment("equation:" + newline + Tran.guard.Text) + newline;
            guardStr = guardC + guardOptStr + "guard = mptPolytope(guardOpt);" + newlines(2);
            
            % Write Transition String
            transStr = "trans{" + num2str(trans) + "} = transition(guard, reset, " +...
                        transDestination + ", 'dummy', 'names');" + newlines(2);
            % Append Transition string
            transitionStr = transitionStr + resetStr + guardStr + transStr;
            
        end
        
        % Write State String
        locStr = "loc{" + num2str(state) + "} = location('S" + num2str(state) +...
                    "'," + num2str(state) + ", inv, trans, dynamics);" + newlines(4);
        % Append State String
        stateStr = stateStr + dynamicsStr + invariantStr + transitionStr + locStr;
        % Append State String to Component String
        componentStr = componentStr + stateStr;
        
    end
    
    if numberOfComp > 1
        parallelCompStr = "comp{" + num2str(comp) + "} = hybridAutomaton(loc);" + newlines(2);
        
        % declare stateBinds
        totalStates = totalStates + length(Comp.states);
        stateBindStr = "sBinds{" + comp + "} = [" + num2str(totalStates - length(Comp.states) + 1);
        for curr=2:length(Comp.states)
            stateBindStr = stateBindStr + "; " + num2str(totalStates - length(Comp.states) + curr);
        end
        stateBindStr = stateBindStr + "];" + newlines(2);
        
        % declare inputBinds
        % default string for comp without inputs
        inputBindStr = "iBinds{" + comp + "} = [];" + newlines(2);
        % syntax of inputBinds: [[origin,#output-of-origin];[x,x];[y,y];...];
        if ~isempty(Comp.inputs) && ~strcmp(Comp.inputs(1).name,'uDummy')
            inputBindStr = "iBinds{" + comp + "} = [[";
            % loop over all inputs
            for inp=1:length(Comp.inputs)
                if inp > 1
                    inputBindStr = inputBindStr + ";[";
                end
                inputName = Comp.inputs(inp).name;
                % default: input comes from composed system
                origin = 0;
                outputOfOrigin = 1;
                % search for inputName in all other components
                for c=1:length(Components)
                    % exception: same as current component
                    if c ~= comp
                        for zzz=1:size(Components{c}.states,2)
                            if contains(inputName,Components{c}.states(zzz).name)
                                origin = c;
                                % output of origin from C matrix?
                                outputOfOrigin = 1;
                                break;
                            end
                        end
                    end
                    if origin
                        % end loop if input source found
                        break;
                    end
                end
                % write bind
                inputBindStr = inputBindStr + num2str(origin) + "," +...
                    num2str(outputOfOrigin) + "]";
            end
            inputBindStr = inputBindStr + "];" + newlines(2);
        end
        
        % put all together
        componentStr = componentStr + parallelCompStr + stateBindStr + inputBindStr;
    end
    
end
if numberOfComp == 1
% If the number of Components is 1, we have a monolithic Automaton   
    aStr = "HA = hybridAutomaton(loc);" + newlines(2);
else
    % If the number of Components is > 1, we have a parallel hybrid Automaton
    aStr ="HA = parallelHybridAutomaton(comp,sBinds,iBinds);" + newlines(2);
end

%optionStr = padComment("Options");


HA = automatonStr + componentStr + aStr + newline + "end";

end

%-----------STRING HELPER FUNCTIONS-------------

function str = padComment(comment,maxLineLength)
%pads comment left & right with dashes to desired length and prefixes "%"

if(nargin<2)
    maxLineLength = 75;
end

lenComment = strlength(comment);
lenLeft = floor((maxLineLength - lenComment)/2) - 1;
lenRight = maxLineLength - lenLeft - lenComment;

str = "%" + repmat('-',1,lenLeft-1) + comment + repmat('-',1,lenRight);

end

function str = newlines(lines)
% fast way to write newline() + newline() + ...
str = string(repmat(newline(),1,lines));
end

% transform possibly multi-line text to comment
function str = text2comment(text)
% format in:
%   "line1
%    line2
%    line3"
% format out:
%   "%% line1
%    %   line2
%    %   line3"
str = "%% " + strrep(text,newline,newline + "%   ");
end
