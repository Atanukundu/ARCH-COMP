#!/usr/bin/python3

import re
import subprocess

verisig_path = '/usr/app/verisig/verisig'
flowstar_path = '/usr/app/verisig/flowstar/flowstar'

print("Building the base model...")
subprocess.run([verisig_path, '-o' ,'-nf', 'ACC.xml', 'tanh.yml'])

with open('ACC.model', 'r') as f:
    model = f.read()

p = re.compile(r'\s+-\s+([\w\d]+) in \[\s*([-]*\d+[\.]*\d+),\s+([-]*\d+[\.]*\d+)\]')

x1 = '[ 90, 91 ]'
x2 = '[ 32, 32.05]'
x3 = '[ 0, 0]'
x4 = '[ 10, 10.5]'
x5 = '[ 30, 30.05]'
x6 = '[ 0, 0]'
aego = '[ 0, 0]'
worldtime = '[ 0, 0]'

print("=========================================")
print("Running test with initial conditions of: ")
print("x1 = [90, 91], x2 = [32, 32.05], x3 = [0,0], x4 = [10, 10.5], x5 = [30, 30.05], x6 = [0,0]")
print("=========================================")

for i in range(0,50,2):
    test_model = model.replace("X1_INTERVAL", x1)
    test_model = test_model.replace("X2_INTERVAL", x2)
    test_model = test_model.replace("X3_INTERVAL", x3)
    test_model = test_model.replace("X4_INTERVAL", x4)
    test_model = test_model.replace("X5_INTERVAL", x5)
    test_model = test_model.replace("X6_INTERVAL", x6)
    test_model = test_model.replace("A_EGO_INTERVAL", aego)
    test_model = test_model.replace("WORLDTIME_INTERVAL", worldtime)
    test_model = test_model.replace("autosig", "autosig" + str(i))

    if i == 48:
        subprocess.run(flowstar_path , input=test_model, shell=True, universal_newlines=True)
        break
    else:
        subprocess.run(flowstar_path + " > temp", input=test_model, shell=True, universal_newlines=True)

    with open('temp', 'r') as f:
        lines = f.readlines()

    lines = lines[-35:-10]

    for line in lines:
        m = p.match(line)
        if m:
            varname = m.group(1)
            lower = float(m.group(2))
            upper = float(m.group(3))

            if varname == 'x1':
                x1 = '[ ' + str(lower) + ', ' + str(upper) + ' ]'
            elif varname == 'x2':
                x2 = '[ ' + str(lower) + ', ' + str(upper) + ' ]'
            elif varname == 'x3':
                x3 = '[ ' + str(lower) + ', ' + str(upper) + ' ]'
            elif varname == 'x4':
                x4 = '[ ' + str(lower) + ', ' + str(upper) + ' ]'
            elif varname == 'x5':
                x5 = '[ ' + str(lower) + ', ' + str(upper) + ' ]'
            elif varname == 'x6':
                x6 = '[ ' + str(lower) + ', ' + str(upper) + ' ]'
            elif varname == 'a_ego':
                aego = '[ ' + str(lower) + ', ' + str(upper) + ' ]'
            elif varname == 'worldtime':
                worldtime = '[ ' + str(lower) + ', ' + str(upper) + ' ]'

print("=========================================")
print("Compiling Plot:")
print("=========================================")
with open("autosig.plt", 'w') as o:
    for i in range(0,50,2):
        with open("outputs/autosig" + str(i) + '.plt', 'r') as f:
            for line in f:
                o.write(line)