function [sDIM,iDIM,W_ub,tau,xi] = readParams(filename)
    filetext = fileread(filename);
    sDIM = str2double(regexp(filetext,'(?<=#define\s+sDIM\s+)\S+','match'));
    iDIM = str2double(regexp(filetext,'(?<=#define\s+iDIM\s+)\S+','match'));
%     cell = regexp(filetext,'(?<=W_lb\s+=\s+{)\S+(?=})','match');
%     W_lb = (str2num(cell{1}))';
    cell = regexp(filetext,'(?<=W_ub\s+=\s+{)\S+(?=})','match');
    W_ub = (str2num(cell{1}))';
%     cell = regexp(filetext,'(?<=lb\[sDIM\]\s*=\s*{)\S+(?=})','match');
%     lb = (str2num(cell{1}))';
%     cell = regexp(filetext,'(?<=ub\[sDIM\]\s*=\s*{)\S+(?=})','match');
%     ub = (str2num(cell{1}))';
    tau = str2double(regexp(filetext,'(?<=tau\s*=\s*)\S+(?=;)','match'));
    xi = str2double(regexp(filetext,'(?<=xi\s*=\s*)\S+(?=;)','match'));
%     cell = regexp(filetext,['(?<=h\[' num2str(2*sDIM) '\]\s*=\s*{)\S+(?=})'],'match');
%     target = str2num(cell{1});
end